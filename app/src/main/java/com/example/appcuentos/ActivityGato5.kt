package com.example.appcuentos

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_gato1.*
import kotlinx.android.synthetic.main.activity_gato2.*
import kotlinx.android.synthetic.main.activity_gato3.*
import kotlinx.android.synthetic.main.activity_gato4.*
import kotlinx.android.synthetic.main.activity_gato5.*

class ActivityGato5 : AppCompatActivity(){
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_gato5)

        val bundle: Bundle? = intent.extras

        val nombre7 = bundle?.getString("key_nombre")
        val edad7 = bundle?.getString("key_edad")

        bundle?.let { bundleLibriDeNull ->
            val nombre1 = bundleLibriDeNull.getString("key_nombre", "Desconocido")
            val edad1 = bundleLibriDeNull.getString("key_edad", "0")

        }
        btnContinuar5.setOnClickListener{
            val bundle = Bundle()
            bundle.apply {
                putString("key_nombre", nombre7)
                putString("key_edad", edad7)
            }
            val intent = Intent(this, ActivityGato6::class.java).apply {
                putExtras(bundle)
            }
            startActivity(intent)
        }
    }
}
