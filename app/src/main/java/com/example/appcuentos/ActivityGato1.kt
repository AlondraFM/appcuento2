package com.example.appcuentos

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_cuento.*
import kotlinx.android.synthetic.main.activity_gato1.*

class ActivityGato1 : AppCompatActivity(){
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_gato1)

        val bundle: Bundle? = intent.extras

        val nombre3 = bundle?.getString("key_nombre")
        val edad3 = bundle?.getString("key_edad")

        bundle?.let { bundleLibriDeNull ->
            val nombre1 = bundleLibriDeNull.getString("key_nombre", "Desconocido")
            val edad1 = bundleLibriDeNull.getString("key_edad", "0")

            /*txtPropo.text = "$nombre1, disfruta el cuento"*/
        }
        btnContinuar2.setOnClickListener{
            val bundle = Bundle()
            bundle.apply {
                putString("key_nombre", nombre3)
                putString("key_edad", edad3)
            }
            val intent = Intent(this, ActivityGato2::class.java).apply {
                putExtras(bundle)
            }
            startActivity(intent)
        }
    }
}