package com.example.appcuentos

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_gato1.*
import kotlinx.android.synthetic.main.activity_gato2.*
import kotlinx.android.synthetic.main.activity_gato3.*
import kotlinx.android.synthetic.main.activity_gato4.*

class ActivityGato4 : AppCompatActivity(){
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_gato4)

        val bundle: Bundle? = intent.extras

        val nombre6 = bundle?.getString("key_nombre")
        val edad6 = bundle?.getString("key_edad")

        bundle?.let { bundleLibriDeNull ->
            val nombre1 = bundleLibriDeNull.getString("key_nombre", "Desconocido")
            val edad1 = bundleLibriDeNull.getString("key_edad", "0")

            txtComentG4.text = "¡Genial!, ¿cierto?, $nombre1"
        }
        btnContinuar4.setOnClickListener{
            val bundle = Bundle()
            bundle.apply {
                putString("key_nombre", nombre6)
                putString("key_edad", edad6)
            }
            val intent = Intent(this, ActivityGato5::class.java).apply {
                putExtras(bundle)
            }
            startActivity(intent)
        }
    }
}