package com.example.appcuentos

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_lottie.*
import kotlinx.android.synthetic.main.activity_main.*

class ActivityLottie : AppCompatActivity(){
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lottie)

        val bundle: Bundle? = intent.extras

        val nombre8 = bundle?.getString("key_nombre")
        val edad8 = bundle?.getString("key_edad")

        bundle?.let { bundleLibriDeNull ->
            val nombre1 = bundleLibriDeNull.getString("key_nombre", "Desconocido")
            val edad1 = bundleLibriDeNull.getString("key_edad", "0")

            txtComentG2.text = "¡$nombre1, gracias por leer!"
        }
        btnContinuar7.setOnClickListener{
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }
    }
}