package com.example.appcuentos

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_registro.*

class ActivityRegistro : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registro)

        btnRegistro.setOnClickListener{
            val nombre = editTextName.text.toString()
            val edad = editTextEdad.text.toString()

            if(nombre.isEmpty()){
                Toast.makeText(this,"Debe ingresar sus nombre", Toast.LENGTH_LONG).show()
                return@setOnClickListener
            }

            if(edad.isEmpty()){
                Toast.makeText(this,"Debe ingresar su edad",Toast.LENGTH_LONG).show()
                return@setOnClickListener
            }

            val bundle = Bundle()
            bundle.apply {
                putString("key_nombre",nombre)
                putString("key_edad",edad)
            }
            val intent = Intent(this,ActivityDatos::class.java).apply {
                putExtras(bundle)
            }
            startActivity(intent)
        }
    }
}