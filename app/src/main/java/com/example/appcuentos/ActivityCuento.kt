package com.example.appcuentos

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_cuento.*

class ActivityCuento : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cuento)

        val bundle : Bundle? = intent.extras

        val nombre2 = bundle?.getString("key_nombre")
        val edad2 = bundle?.getString("key_edad")

        bundle?.let { bundleLibriDeNull ->
            val nombre1 = bundleLibriDeNull.getString("key_nombre", "Desconocido")
            val edad1 = bundleLibriDeNull.getString("key_edad", "0")

            txtPropo.text = "$nombre1, disfruta el cuento"
        }

        btnLeer.setOnClickListener{
            val bundle = Bundle()
            bundle.apply {
                putString("key_nombre", nombre2)
                putString("key_edad", edad2)
            }
            val intent = Intent(this, ActivityGato1::class.java).apply {
                putExtras(bundle)
            }
            startActivity(intent)
        }
    }
}