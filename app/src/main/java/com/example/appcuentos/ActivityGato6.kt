package com.example.appcuentos

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_gato1.*
import kotlinx.android.synthetic.main.activity_gato2.*
import kotlinx.android.synthetic.main.activity_gato3.*
import kotlinx.android.synthetic.main.activity_gato4.*
import kotlinx.android.synthetic.main.activity_gato5.*
import kotlinx.android.synthetic.main.activity_gato6.*

class ActivityGato6 : AppCompatActivity(){
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_gato6)

        val bundle: Bundle? = intent.extras

        val nombre8 = bundle?.getString("key_nombre")
        val edad8 = bundle?.getString("key_edad")

        bundle?.let { bundleLibriDeNull ->
            val nombre1 = bundleLibriDeNull.getString("key_nombre", "Desconocido")
            val edad1 = bundleLibriDeNull.getString("key_edad", "0")

            txtComentG6.text = "¡Qué hermoso cuento, $nombre1,!"
        }
        btnContinuar6.setOnClickListener{
            val bundle = Bundle()
            bundle.apply {
                putString("key_nombre", nombre8)
                putString("key_edad", edad8)
            }
            val intent = Intent(this, ActivityLottie::class.java).apply {
                putExtras(bundle)
            }
            startActivity(intent)
        }
    }
}