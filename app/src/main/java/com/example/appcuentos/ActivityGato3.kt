package com.example.appcuentos

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_gato1.*
import kotlinx.android.synthetic.main.activity_gato2.*
import kotlinx.android.synthetic.main.activity_gato3.*

class ActivityGato3 : AppCompatActivity(){
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_gato3)

        val bundle: Bundle? = intent.extras

        val nombre5 = bundle?.getString("key_nombre")
        val edad5 = bundle?.getString("key_edad")

        bundle?.let { bundleLibriDeNull ->
            val nombre1 = bundleLibriDeNull.getString("key_nombre", "Desconocido")
            val edad1 = bundleLibriDeNull.getString("key_edad", "0")

            txtComent2.text = "Espero sea de tu agrado, $nombre1"
        }
        btnContinuar.setOnClickListener{
            val bundle = Bundle()
            bundle.apply {
                putString("key_nombre", nombre5)
                putString("key_edad", edad5)
            }
            val intent = Intent(this, ActivityGato4::class.java).apply {
                putExtras(bundle)
            }
            startActivity(intent)
        }
    }
}