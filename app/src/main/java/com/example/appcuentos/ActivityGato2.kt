package com.example.appcuentos

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_gato2.*

class ActivityGato2  : AppCompatActivity(){
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_gato2)

        val bundle: Bundle? = intent.extras

        val nombre4 = bundle?.getString("key_nombre")
        val edad4 = bundle?.getString("key_edad")

        bundle?.let { bundleLibriDeNull ->
            val nombre1 = bundleLibriDeNull.getString("key_nombre", "Desconocido")
            val edad1 = bundleLibriDeNull.getString("key_edad", "0")

            txtComentA2.text = "Interesante, ¿no?, $nombre1"
        }
        btnContinuar3.setOnClickListener{
            val bundle = Bundle()
            bundle.apply {
                putString("key_nombre", nombre4)
                putString("key_edad", edad4)
            }
            val intent = Intent(this, ActivityGato3::class.java).apply {
                putExtras(bundle)
            }
            startActivity(intent)
        }
    }
}