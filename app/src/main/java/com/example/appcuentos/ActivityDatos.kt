package com.example.appcuentos

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_datos.*
import kotlinx.android.synthetic.main.activity_main.*

class ActivityDatos : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_datos)

        // Obtenemos el bundle
        val bundle : Bundle? = intent.extras

        val nombre1 = bundle?.getString("key_nombre")
        val edad1 = bundle?.getString("key_edad")

        bundle?.let {bundleLibriDeNull ->
            val nombre = bundleLibriDeNull.getString("key_nombre","Desconocido")
            val edad = bundleLibriDeNull.getString("key_edad","0")

            txtNombreDatos.text = "¡Hola! $nombre"
            txtEdadDatos2.text = "A tus $edad años te contaré un cuento"
        }
        btnIniciar.setOnClickListener{
            val bundle = Bundle()
            bundle.apply {
                putString("key_nombre", nombre1)
                putString("key_edad", edad1)
            }
            val intent = Intent(this, ActivityCuento::class.java).apply {
                putExtras(bundle)
            }
            startActivity(intent)
        }
    }
}